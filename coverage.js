function initMap() {
    var prague = {lat: 50.08804, lng: 14.42076};
    map = new google.maps.Map(document.getElementById('map'), {
      zoom: 12,
      center: prague
    });
    marker = new google.maps.Marker({
      position: prague,
      map: map
    });
}

// Fix layout
$('#map').height($('main').height())
$('#search').submit(false);
$(".dropdown-trigger").dropdown();
$(document).ready(function(){
    $('select').select();
  });

// Parse user input
$("#search-input").keyup(function(event) {
    if (event.keyCode === 13) {
        M.toast({html: 'Zjišťuji pokrytí...'})
        $.ajax({
          "async": true,
          "crossDomain": true,
          "url": "https://coverage.api.simplecell.eu/get_coverage",
          "method": "POST",
          "headers": {
            "content-type": "application/x-www-form-urlencoded",
          },
          "data": {
            "key": "<AUTH_KEY>",
            "use_case": $("#use-case").val(),
            "coverage_type": $("#coverage-type").val(),
            "lang": "cs",
            //"lat": "50.0502778",
            //"lng": "14.4361111",
            "address": $("#search-input").val()
          }
        }).done(function (response) {
          console.log(response)
          if (!response.hasOwnProperty("predicted_lat")) {
            M.toast({html: 'Chyba. Nelze zjistit souřadnice z dané adresy.'})
            return false;
          } 

          // Map reposition
          var newLatLng = new google.maps.LatLng(response.predicted_lat, response.predicted_lng);
          marker.setPosition(newLatLng);
          map.setCenter(marker.getPosition());

          // Infowindow
          if (typeof infowindow !== "undefined"){
            infowindow.close()
          }

          infowindow = new google.maps.InfoWindow({
              content: "Kvalita pokrytí: <b>" + response.quality + "</b><br>" + "Úrovně SNR: " + response.snr
          });
          infowindow.open(map,marker);

          // Radius
          if (typeof radius !== "undefined"){
            radius.setMap(null)
          }
          radius = new google.maps.Circle({
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0.35,
            map: map,
            center: marker.getPosition(),
            radius: response.radius
          });

        });
    }
});

