# SimpleCell Coverage Demo

Basic web application that utilizes SimpleCell Coverage API and renders results to a user.
Feel free to reuse the app as-is or use its parts to build your own application.

## How-to
* Ask for [SimpleCell Coverage API key](https://simplecell.eu/kontakt)
* Get the [Google Maps API key](https://developers.google.com/maps/documentation/javascript/get-api-key) - step 1
* Download a copy of the app
* Open **coverage.html** and replace **<GOOGLE_MAPS_API_KEY>** with Google Maps API key
* Open **coverage.js** and replace **<AUTH_KEY>** with SimpleCell Coverage API key
* Deploy the app into your webserver

## Support
* [Email](mailto:support@simplecell.eu)



